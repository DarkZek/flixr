FROM rust:1.39.0-slim

WORKDIR /opt/background_service/
COPY ./background_service .

RUN apt update
RUN apt install msmtp libssl-dev pkg-config -y
RUN cargo install --path .

CMD ["background_service"]