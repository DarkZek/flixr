#!/bin/bash
echo "Setting up development environment"
echo "Installing dependencies"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-get install software-properties-common -y
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get -y install docker-ce curl git nodejs docker npm software-properties-common
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update
sudo apt-get install php7.3-cli php7.3 php7.3-mbstring php7.3-dom -y

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo groupadd docker
sudo gpasswd -a $USER docker

sudo dockerd
cp -n ./build/vhost.conf.example ./build/vhost.conf

docker-compose up -d && docker-compose down

./build/scripts/msmtp_configure.sh

./build/scripts/database_configure.sh run-laravel

mkdir ./website/shows/
chmod 777 -R ./website/shows
rm ./website/bootstrap/cache/config.php

docker-compose up -d
xdg-open http://localhost:8880/ > /dev/null