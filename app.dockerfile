FROM php:7.3.0-fpm

RUN apt-get update && apt-get install ca-certificates software-properties-common -y \
    && add-apt-repository -y ppa:ondrej/php && apt-get install -y libmcrypt-dev \
    mysql-client --no-install-recommends msmtp mailutils \
    && docker-php-ext-install pdo_mysql

# Set up php sendmail config
RUN echo "sendmail_path = '/usr/bin/msmtp -C /var/msmtp.conf -t'" >> /usr/local/etc/php/conf.d/php-sendmail.ini

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini