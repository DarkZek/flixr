#!/bin/bash
echo "Would you like to run dev setup first?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                ./dev-setup.sh
                break;;
            No ) 
                break;;
        esac
    done

cp ./build/prod-docker-compose.yml ./docker-compose.override.yml

echo "Changing configs for production..."

sed -i -e "s/APP_DEBUG=true/APP_DEBUG=false/g" ./website/.env
docker exec app bash -c "cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini"

cd ./website

composer install --optimize-autoloader --no-dev
php artisan config:cache
php artisan route:cache

npm run production

cd ..
