use std::collections::HashMap;
use crate::reminder_emails::EmailReminder;
use crate::omdb_fetcher::ReleasingShow;
use chrono::{Datelike, Utc};
use mysql::Pool;

pub fn calculate_reminders(reminders: &HashMap<String, EmailReminder>) -> HashMap<String, EmailReminder> {
    let mut day_reminders: HashMap<String, EmailReminder> = HashMap::new();

    for (email, reminder) in reminders.iter() {
        for show in reminder.shows.iter() {
            if show.release_date.unwrap().ordinal() == Utc::now().ordinal() {

                // Releasing today
                if !day_reminders.contains_key(email){

                    let email_reminder = EmailReminder {
                        email: reminder.email.clone(),
                        shows: Vec::new()
                    };

                    day_reminders.insert(email.clone(), email_reminder);
                }

                day_reminders.get_mut(email).unwrap().shows.push((*show).clone());

            }
        }
    }

    day_reminders
}