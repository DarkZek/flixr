extern crate mysql;

use self::mysql::Pool;
use std::env;
use std::error::Error;
use crate::omdb_fetcher::ReleasingShow;
use crate::reminder_emails::EmailReminder;
use std::collections::HashMap;

pub struct Credentials {
    username: String,
    password: String,
    host: String,
    port: String,
    database: String
}

pub struct Reminder {
    email: String,
    show_id: String
}

pub fn get_user_reminders(releasing_shows: &Vec<ReleasingShow>, db: &Pool) -> Result<HashMap<String, EmailReminder>, Box<dyn Error>> {

    let reminders: Vec<Reminder> =
        db.prep_exec("SELECT email, show_id FROM subscriptions", ())
            .map(|result| {
                result.map(|x| x.unwrap()).map(|row| {
                    let (email, show_id) = mysql::from_row(row);
                    Reminder {
                        email,
                        show_id
                    }
                }).collect()
            })?;

    let mut email_reminders: HashMap<String, EmailReminder> = HashMap::new();

    for show in releasing_shows {
        for reminder in &reminders {
            if reminder.show_id == show.imdb_key {

                //Create if doesn't exist
                if !email_reminders.contains_key(reminder.email.as_str()) {
                    let mut email_reminder = EmailReminder {
                        email: reminder.email.clone(),
                        shows: Vec::new()
                    };

                    email_reminder.shows.push(show.clone());

                    email_reminders.insert(reminder.email.clone(), email_reminder);
                } else {
                    let email_reminder = email_reminders.get_mut(reminder.email.as_str());
                    let reminders = email_reminder.unwrap();
                    let shows = &mut reminders.shows;

                    shows.push(show.clone());
                }
            }
        }
    }

    Ok(email_reminders)
}

pub fn database_connect(creds: Credentials) -> Result<Pool, mysql::Error> {
    let connection_string = format!("mysql://{}:{}@{}:{}/{}", creds.username, creds.password, creds.host, creds.port, creds.database);

    Ok(mysql::Pool::new(connection_string)?)
}

pub fn get_credentials() -> Credentials{
    return Credentials {
        username: env::var("MYSQL_USER").unwrap_or_default(),
        password: env::var("MYSQL_PASSWORD").unwrap_or_default(),
        host: env::var("MYSQL_HOST").unwrap_or_default(),
        port: env::var("MYSQL_PORT").unwrap_or_default(),
        database: env::var("MYSQL_DATABASES").unwrap_or_default()
    };
}