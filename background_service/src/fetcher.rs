extern crate reqwest as requests;
extern crate rss;

use std::fs::File;
use std::io::{Write, Read};
use std::io;
use std::env;
use std::io::BufReader;
use rss::Channel;
use std::error::Error;
use crate::settings::SETTINGS;
use mysql::Pool;
use chrono::{Utc, Datelike};

pub fn fetch_latest_shows(db: &Pool) -> Result<Channel, Box<dyn Error>>{
    let resp = requests::get(SETTINGS.rss_url);

    let mut xml = match resp {
        Ok(x) => x,
        Err(e) => return Err(Box::new(e))
    };

    // Compare
    let mut current_rss = String::new();
    let new_rss = xml.text().unwrap();

    File::open(SETTINGS.rss_file_path).unwrap().read_to_string(&mut current_rss).unwrap();

    if Utc::now().day() == 0 {
        db.prep_exec("DELETE FROM reminders_sent;", ()).unwrap();
    }

    //Write to file
    if let Err(e) = save_to_file(new_rss) {
        return Err(Box::new(e));
    }

    match parse_rss_feed() {
        Ok(rss) => Ok(rss),
        Err(e) => Err(Box::new(e))
    }
}

fn save_to_file(rss_feed: String) -> Result<(), io::Error>{

    let path = env::current_dir()?;
    println!("Saving file to {}{}{}", path.display(), "/", SETTINGS.rss_file_path);

    let f = File::create(SETTINGS.rss_file_path);

    match f {
        Ok( mut f) => {

            if let Err(e) = f.write_all(rss_feed.as_ref()) {
                return Err(e);
            }

            if let Err(e) = f.sync_all() {
                println!("Error syncing filesystem: {}", e);
            }
        },
        Err(e) => return Err(e)
    }

    Ok(())
}

fn parse_rss_feed() -> Result<Channel, rss::Error>{
    let file = File::open(SETTINGS.rss_file_path).unwrap();
    return Channel::read_from(BufReader::new(file));
}