use std::collections::HashMap;
use std::ops::Add;
use std::{str};
use crate::reminder_emails::EmailReminder;
use crate::reminder_emails;
use mysql::Pool;

const EMAIL_HEADER: &'static str = include_str!("../assets/day_header.html");
const EMAIL_CONTENT: &'static str = include_str!("../assets/day_content.html");
const EMAIL_FOOTER: &'static str = include_str!("../assets/day_footer.html");
const EMAIL_SENDER: &'static str = "noreply@flixrapp.com";

pub fn send_day_email(reminders: HashMap<String, EmailReminder>, db: &Pool) {
    for reminder in reminders {
        generate_email(reminder.1, db);
    }
}

pub fn generate_email(reminder: EmailReminder, db: &Pool) {
    let email_address = reminder.email;
    let mut email = String::from(EMAIL_HEADER);

    let mut displayed_shows = 0;

    for show in &reminder.shows {

        let rows = db.prep_exec("SELECT * FROM reminders_sent WHERE email = :email AND show_id = :show_id AND reminder_stage > 1",
                                params! {
                                    "email" => email_address.clone(),
                                    "show_id" => show.imdb_key.clone()
                                 })
            .unwrap().count();

        if rows > 0 {
            // Already reminded them today
            continue;
        }

        let mut content = String::from(EMAIL_CONTENT);
        content = content.replace("%SHOW_TITLE%", show.get_stripped_name().as_str());
        content = content.replace("%SHOW_DESCRIPTION%", show.description.as_str());
        content = content.replace("%SHOW_POSTER%", reminder_emails::get_poster_url(show.imdb_key.clone()).as_str());
        email = email.add(content.as_str());

        displayed_shows += 1;

        // Update row
        db.prep_exec(r"UPDATE reminders_sent SET reminder_stage = 2 WHERE email = :email AND show_id = :show_id",
                     params! { "email" => email_address.clone(), "show_id" => show.imdb_key.clone() });
    }

    if displayed_shows == 0 {
        return;
    }

    email = email.replace("%SHOWS_COUNT%", format!("{}", displayed_shows).as_str());

    email = email.add(EMAIL_FOOTER);

    let subject = if &reminder.shows.len() > &1 {
        "You have some shows releasing today!"
    } else {
        "You have a show releasing today!"
    };

    if let Err(e) = reminder_emails::send_email(subject.to_string(), email_address.clone(), email) {
        println!("{}", e);
    }
}