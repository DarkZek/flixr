use std::time::Duration;
use std::thread;
use crate::settings::SETTINGS;
use crate::reminder::{database_connect, get_credentials};

mod fetcher;
mod settings;
mod reminder;
mod command_line;
mod omdb_fetcher;
mod reminder_emails;
mod release_day_reminders;
mod release_day_reminder_emails;

#[macro_use]
extern crate mysql;

fn main() {

    if command_line::verify_args() {
        return;
    }

    println!("Fetching latest tv shows from metacritic");

    let db = database_connect(get_credentials()).expect("Couldn't connect to database");

    //Loop until we get a valid response
    let fetched_response = loop {
        match fetcher::fetch_latest_shows(&db) {
            Ok(x) => break x,
            Err(e) => println!("{}", e)
        }

        thread::sleep(Duration::from_millis(SETTINGS.network_retry_time));
        println!("Retrying connection...");
    };

    let new_shows = omdb_fetcher::fetch_shows(fetched_response);

    let monthly_reminders = match reminder::get_user_reminders(&new_shows, &db) {
        Ok(reminders) => reminders,
        Err(e) => {
            println!("Couldn't send user reminders: {}", e);
            return;
        }
    };

    let daily_reminders = release_day_reminders::calculate_reminders(&monthly_reminders);

    // This one needs to be first so that SQL is in the right order
    reminder_emails::send_reminder_emails(monthly_reminders, &db);
    release_day_reminder_emails::send_day_email(daily_reminders, &db);

    //Print the response
    println!("Success!");
}
