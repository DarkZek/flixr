use std::env;

pub fn verify_args() -> bool{
    let env_args: Vec<_> = env::args().collect();

    if env_args.len() == 0 {
        return verify_args();
    }

    //Check for help command
    if env_args[0] == "--help" || env_args[0] == "-h" {
        println!("Flixr Background Service");
        println!("v0.1 by Marshall Ashdowne");
        println!("-h --help     | Lists help information");
        println!("-o --override | Overrides time check and forces");
        println!("new emails to be sent.");
        return true;
    }


    return verify_vars();
}

fn verify_vars() -> bool {

    let vars = ["MYSQL_USER", "MYSQL_PASSWORD", "MYSQL_HOST", "MYSQL_PORT", "MYSQL_DATABASES", "OMDB_API_KEY"];

    for var in &vars {
        if let Err(_) = env::var(var) {
            println!("{} environment variable not set", var);
            return true;
        }
    }

    return false;
}