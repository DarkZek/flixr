use crate::omdb_fetcher::ReleasingShow;
use std::collections::HashMap;
use std::ops::Add;
use std::process::{Command, Stdio};
use std::io::{Error, Write};
use std::{str, fs};
use crate::settings::SETTINGS;
use std::fs::File;
use mysql::Pool;

pub struct EmailReminder {
    pub email: String,
    pub shows: Vec<ReleasingShow>
}

const EMAIL_HEADER: &'static str = include_str!("../assets/reminder_header.html");
const EMAIL_CONTENT: &'static str = include_str!("../assets/reminder_content.html");
const EMAIL_FOOTER: &'static str = include_str!("../assets/reminder_footer.html");
const EMAIL_SENDER: &'static str = "noreply@flixrapp.com";
const EMAIL_SUBJECT: &'static str = "You have new shows releasing!";

pub fn send_reminder_emails(reminders: HashMap<String, EmailReminder>, db: &Pool) {
    // If debug mode we only save emails as files, so work out directory for them
    if SETTINGS.debug_mode {
        fs::remove_dir("./emails/");
        fs::create_dir("./emails/");
    }

    for reminder in reminders {
        generate_email(reminder.1, db);
    }
}

pub fn generate_email(reminder: EmailReminder, db: &Pool) {
    let email_address = reminder.email;
    let mut email = String::from(EMAIL_HEADER);

    let mut displayed_shows = 0;

    for show in reminder.shows {

        let rows = db.prep_exec("SELECT * FROM reminders_sent WHERE email = :email AND show_id = :show_id",
                                params! {
                                    "email" => email_address.clone(),
                                    "show_id" => show.imdb_key.clone()
                                 })
            .unwrap().count();

        // Already sent at least one email
        if rows > 0 {
            continue;
        }

        let mut content = String::from(EMAIL_CONTENT);
        content = content.replace("%SHOW_TITLE%", show.get_stripped_name().as_str());
        content = content.replace("%SHOW_DESCRIPTION%", show.description.as_str());
        content = content.replace("%SHOW_POSTER%", get_poster_url(show.imdb_key.clone()).as_str());

        email = email.add(content.as_str());

        let show_id = show.imdb_key;

        println!("{} has a new releasing show: {}", email_address.clone(), show.title);

        displayed_shows += 1;

        db.prep_exec(r"INSERT INTO reminders_sent (email, show_id, reminder_stage) VALUES (:email, :show_id, 1)",
                     params! { "email" => email_address.clone(), show_id });
    }

    if displayed_shows == 0 {
        return;
    }

    email = email.replace("%SHOWS_COUNT%", format!("{}", displayed_shows).as_str());
    email = email.add(EMAIL_FOOTER);

    if let Err(e) = send_email(EMAIL_SUBJECT.to_string(), email_address.clone(), email) {
        println!("{}", e);
    }
}

pub(crate) fn get_poster_url(imdb_url: String) -> String {
    return format!("https://flixrapp.com/shows/{}/poster.jpeg", imdb_url);
}

pub(crate) fn send_email(subject: String, recipient: String, email: String) -> Result<(), Error> {
    let email = format!("From:The Flixr Team <{}>\nContent-Type: text/html\nSubject:{}\n\n{}", EMAIL_SENDER, subject, email);

    // TODO: Write html files instead if in debug mode
    if SETTINGS.debug_mode {
        let addr = recipient.split_at(recipient.find("@").unwrap());

        // Clip off the email junk if we save to file
        let email = email.split_at(email.find("<html>").unwrap());

        File::create(format!("./emails/{}.html", addr.0))
            .unwrap()
            .write_all(email.1.as_bytes()).unwrap();
    } else {

        let mut cmd = Command::new("/usr/bin/msmtp")
            .arg("-C")
            .arg("/var/msmtp.conf")
            .arg(recipient)
            .stdin(Stdio::piped())
            .spawn()?;

        {
            let child_stdin = cmd.stdin.as_mut().unwrap();
            child_stdin.write_all(email.as_bytes())?;
        }

        if let Err(e) = cmd.wait_with_output() {
            println!("{}", e);
        }
    }

    Ok(())
}