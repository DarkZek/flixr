pub struct SettingsStruct {
    pub rss_file_path: &'static str,
    pub rss_url: &'static str,
    pub network_retry_time: u64,
    pub(crate) debug_mode: bool
}

pub static SETTINGS: SettingsStruct = SettingsStruct {
    rss_file_path: "metacritic.xml",
    rss_url: "https://www.metacritic.com/rss/tv",
    network_retry_time: 2000,
    debug_mode: false
};