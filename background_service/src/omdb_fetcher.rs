extern crate regex;
extern crate reqwest as requests;
extern crate simple_error;

use regex::Regex;
use rss::Channel;
use std::error::Error;
use std::env;
use self::simple_error::SimpleError;
use chrono::{DateTime, FixedOffset};

#[derive(Clone)]
pub struct ReleasingShow {
    pub title: String,
    pub description: String,
    pub imdb_key: String,
    pub release_date: Option<DateTime<FixedOffset>>
}

impl ReleasingShow {
    pub fn get_stripped_name(&self) -> String {
        //Remove any extra whitespace
        let mut title = self.title.trim();

        let remove_parenthesis = Regex::new(r"\(.*\)").unwrap();
        let title_p_remove = remove_parenthesis.replace_all(title, "");
        title = title_p_remove.trim();

        let remove_season= Regex::new(r"[:]?[ ]?Season [0-9]+$").unwrap();
        let title_s_remove = remove_season.replace_all(title, "");
        title = title_s_remove.trim();

        title.to_owned()
    }
}

pub fn fetch_shows(rss_feed: Channel) -> Vec<ReleasingShow>{

    let mut shows: Vec<ReleasingShow> = Vec::new();

    for show in rss_feed.items() {

        let title = show.title().unwrap_or("");
        let description = show.description().unwrap_or("");
        let release = show.pub_date().unwrap_or("");

        let release_date = DateTime::parse_from_rfc2822(release).ok();

        shows.push(ReleasingShow {
            title: String::from(title),
            description: String::from(description),
            imdb_key: String::new(),
            release_date
        });
    }

    for show in &mut shows {
        match get_imdb_info(show.get_stripped_name()) {
            Ok(info) => {
                show.imdb_key = info;
            },
            Err(e) => {
                println!("Failed to get info for {}: {}", show.get_stripped_name(), e);
            }
        }
    }

    shows
}

fn get_imdb_info(name: String) -> Result<String, Box<dyn Error>>{
    let url = format!("http://www.omdbapi.com/?type=series&apikey={}&t={}", env::var("OMDB_API_KEY").unwrap_or_default(), name);

    let resp = requests::get(&url);

    let mut search = match resp {
        Ok(x) => x,
        Err(e) => return Err(Box::new(e))
    };

    let id_extract= Regex::new("imdbID\":\"(.*?)\"").unwrap();

    match id_extract.captures(search.text().unwrap().as_str()) {
        Some(x) => {
            return Ok(String::from(x.get(1).unwrap().as_str()));
        },
        None => {
            return Err(Box::new(SimpleError::new("No ID found")));
        }
    }
}
