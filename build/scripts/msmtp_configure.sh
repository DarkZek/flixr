#!/bin/bash
echo "Configuring New Setup"

echo "--== Email Configuration ==--"

SMTP_URL=$'\n'

while [ "$SMTP_URL" = $'\n' ]; do
    read -p "SMTP URL: " SMTP_URL
done

echo "Using SMTP server $SMTP_URL"







SMTP="None"

while [ $(echo "$SMTP" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ] ; do
    read -p "SMTP Port (Default 587): " SMTP

    if [ $SMTP=="\n" ]; then SMTP="587"; fi;
done

echo "Using port $SMTP"




echo
EMAIL="None"

while ! [[ "$EMAIL" =~ ^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$ ]] ; do
    read -p "Email used to send emails: " EMAIL
done

echo "Using address $EMAIL"






USERNAME=$'\n'

while [ "$USERNAME" = $'\n' ]; do
    read -p "Email Username: " USERNAME

done

echo "Using username $USERNAME"






PASSWORD=$'\n'

while [ "$PASSWORD" = $'\n' ]; do
    read -p "Email Password: " PASSWORD
done

echo "Using password $PASSWORD"

echo "Writing email configuration to msmtp.conf"

#Replace variables in file
cp ./build/msmtp.example.conf ./build/msmtp.conf

sed -i -e "s/%USERNAME%/$USERNAME/g" ./build/msmtp.conf
sed -i -e "s/%PASSWORD%/$PASSWORD/g" ./build/msmtp.conf
sed -i -e "s/%EMAIL%/$EMAIL/g" ./build/msmtp.conf
sed -i -e "s/%SMTP%/$SMTP_URL/g" ./build/msmtp.conf
sed -i -e "s/%PORT%/$SMTP/g" ./build/msmtp.conf

echo '#This file was auto generated. Feel free to change it, it will automatically update without restarting the containers' | cat - ./build/msmtp.conf > temp && mv temp ./build/msmtp.conf

echo "Done email configuration"