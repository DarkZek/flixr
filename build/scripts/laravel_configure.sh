#!/bin/bash
echo "--== Laravel Configuration ==--"

cd ./website

npm update && npm install --save popper.js bootstrap jquery vue vuex

npm run dev

composer update && composer install

echo "Copying database setup into laravel"

if [ ! -f ./.env ]; then
    echo "Creating env file"
    cp ./.env.example ./.env
fi

docker exec app bash -c "cd /var/www/ && php artisan key:generate"


DB=$(grep -n 'OMDB_API_KEY' ./.env | head -n 1)
if [ -z "${DB#*=}" ]; then
    OMDB=$'\n'

    while [ "$OMDB" = $'\n' ]; do
        read -p "OMDB API KEY: " OMDB
    done

    echo "Using OMDB Key $OMDB"

    #Replaces the line that contains OMDB_API_KEY with the proper key
    OMDB_LINE=$(grep -n 'OMDB_API_KEY' ./.env | head -n 1 | cut -d: -f1);
    sed -i "${OMDB_LINE}s/.*/OMDB_API_KEY=$OMDB/" ./.env
fi

#Copy over the database credentials
MYSQL_USER=$(grep 'MYSQL_USER' ../build/database.env | head -n 1);
MYSQL_USER=${MYSQL_USER#*=}
MYSQL_PASSWORD=$(grep 'MYSQL_PASSWORD' ../build/database.env | head -n 1);
MYSQL_PASSWORD=${MYSQL_PASSWORD#*=}

MYSQL_USER_LINE=$(grep -n 'DB_USERNAME' ./.env | head -n 1 | cut -d: -f1);
sed -i "${MYSQL_USER_LINE}s/.*/DB_USERNAME=$MYSQL_USER/" ./.env

MYSQL_PASSWORD_LINE=$(grep -n 'DB_PASSWORD' ./.env | head -n 1 | cut -d: -f1);
sed -i "${MYSQL_PASSWORD_LINE}s/.*/DB_PASSWORD=$MYSQL_PASSWORD/" ./.env

cd ..