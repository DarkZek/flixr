#!/bin/bash
echo "--== Database Configuration ==--"

AskInformation() {
    echo
    ROOT_PASSWORD="None"

    while ! [[ "$ROOT_PASSWORD" =~ ^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$ ]] ; do
        read -p "Root password: " ROOT_PASSWORD
    done

    echo "Using root password $ROOT_PASSWORD"




    echo
    PASSWORD="None"

    while ! [[ "$PASSWORD" =~ ^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$ ]] ; do
        read -p "Email used to send emails: " PASSWORD
    done

    echo "Using address $PASSWORD"
    echo
    ROOT_PASSWORD="None"




    while ! [[ "$USERNAME" =~ ^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$ ]] ; do
        read -p "Username: " USERNAME
    done

    echo "Using username $USERNAME"

    $script:USERNAME="$USERNAME"
    $script:ROOT="$ROOT_PASSWORD"
    $script:PASSWORD="$PASSWORD"
}

if [ ! -f ./build/database.env ]; then

    echo "Would you like to automatically generate users passwords?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) 
                PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 60);
                ROOT=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 60);
                USERNAME="flixr";
                break;;
            No ) 
            
                AskInformation

            break;;
        esac
    done

    echo "Writing to database.env"
    cp ./build/database.env.example ./build/database.env

    sed -i -e "s/%ROOT%/$ROOT/g" ./build/database.env
    sed -i -e "s/%PASSWORD%/$PASSWORD/g" ./build/database.env
    sed -i -e "s/%USER%/$USERNAME/g" ./build/database.env

    echo "Writing to setup.sql"
    cp ./build/setup.sql.example ./build/setup.sql
    sed -i -e "s/%PASSWORD%/$PASSWORD/g" ./build/setup.sql

    docker-compose stop
    docker-compose rm -f
    sleep 2
    docker volume rm flixr_dbdata
else
    echo "No need to create new credentials, already set. If you want to recreate database credentials delete /build/database.env"
    #Read old credentials
    ROOT=$(grep 'MYSQL_ROOT_PASSWORD' ./build/database.env | head -n 1);
    ROOT=${ROOT#*=}
fi

#Setup database
echo "Setting up database..."
docker-compose up -d

sleep 10

docker exec database bash -c "mysql --user='root' --password='$ROOT' < /etc/setup.sql"

if [ $# -eq 0 ]
then
    echo "Skipping laravel configure"
else
    ./build/scripts/laravel_configure.sh
fi

cd ./website

echo "Migrating database"
docker exec app bash -c "cd /var/www/ && php artisan migrate"

cd ..

docker-compose stop

echo "Done database configuration"