<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/search', 'ShowsController@Search');
Route::post('/save', 'ShowsController@VerifiedSaveShows');
Route::post('/subscribe', 'EmailController@Subscribe');
Route::get('/verify', 'EmailController@Verify');
Route::get('/randomposter', 'ShowsController@RandomPoster');

Route::get('/verified', function () {
    return view('verified');
});