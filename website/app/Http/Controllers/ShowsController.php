<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\SpamHelper;
use Intervention\Image\Facades\Image;

class ShowsController extends Controller
{

    public function Search(Request $request) {

        SpamHelper::CheckIfBlocked();
        
        $API_KEY = getenv("OMDB_API_KEY"); 
        $query = $_GET["q"];

        if (is_null($query)) {
            die("{ \"message\": \"No search query provided\" }");
        }

        if (strlen($query) <= 2) {
            die("{ \"message\": \"Query too short (min 3)\" }");
        }

        $query = str_replace( ' ', "%20", $query);
        $url = "http://www.omdbapi.com/?plot=short&type=series&apikey=" . $API_KEY . "&s=" . $query;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result, true);

        if ($json["Response"] === "False") {
            die("{ \"message\": \"No results\" }");
        }

        $response = [];
        foreach($json['Search'] as $show) {
            ShowsController::CacheShow($show);
            array_push($response, $show["imdbID"]);
        }

        return $response;
    }

    public function CacheShow($show) {

        $shows_storage = getenv("SHOWS_STORAGE_LOCATION") . $show["imdbID"] . "/";

        if (file_exists($shows_storage)) {
            //Exists
            return;
        } else {
            //New show!, create directory for it
            mkdir($shows_storage);

            //We need to fetch poster of it
            if ($show["Poster"] !== "N/A") {
                $poster = file_get_contents($show["Poster"]);
                file_put_contents($shows_storage . "poster.jpeg", $poster);
            }

            $info = array("name" => "Untitled", "year" => "2000");
        }

        //Get plot & rating
        $API_KEY = getenv("OMDB_API_KEY"); 
        $url = "http://www.omdbapi.com/?&type=series&apikey=" . $API_KEY . "&i=" . $show["imdbID"];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_USERAGENT,"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result, true);

        if ($json["Response"] === "False") {
            //Couldnt fetch data for some reason
            //TODO: Log this somewhere
            return;
        }

        $info = new \stdClass();;
        $info->name = $show["Title"];
        $info->year = $show["Year"];
        $info->rating = $json["imdbRating"];
        $info->description = $json["Plot"];

        file_put_contents($shows_storage . "info.json", json_encode($info));
    }

    public function VerifiedSaveShows() {
        $code = strtolower($_POST["code"]);

        if (is_null($code)) {
            die(" { \"message\": \"Invalid code\" }");
        }

        $subscriptions = $_POST["subscriptions"];

        if (is_null($subscriptions) || strlen($subscriptions) == 0 || strlen($subscriptions) > 300 || !EmailController::SubscriptionsExist($subscriptions)) {
            die(" { \"message\": \"Invalid subscriptions\" }");
        }

        $user_count = \DB::select('SELECT * FROM access_keys WHERE code = :code', ['code' => $code]);

        if (sizeof($user_count) == 0) {
            die(" { \"message\": \"Invalid code\" }");
        }

        $email = $user_count[0]->email;

        //Get current subscriptions
        $activeSubs = \DB::select('SELECT * FROM subscriptions WHERE email = :email', ['email' => $email]);
        $targetSubs = explode(';', $subscriptions);

        //Remove blank target subs
        for ($i = 0; $i <= sizeof($targetSubs); $i++) {
            if ($targetSubs[$i] == "") {
                unset($targetSubs[$i]);
            }
        }

        foreach ($activeSubs as $sub) {
            //Check if it exists in the target subs
            for ($i = 0; $i < sizeof($targetSubs); $i++) {
                print($i);
                $target = $targetSubs[$i];
                if ($sub->show_id === $target) {
                    print($sub->show_id . " " . $target);
                    //Same show, they want to keep it! Delete it out of target subs as it has been dealt with
                    array_splice($targetSubs, $i, 1);
                    continue 2;
                }
            }
            //Couldnt find a match in the target subs, they want it deleted
            \DB::delete('DELETE FROM subscriptions WHERE email = :email AND show_id = :show_id', ['email' => $email, 'show_id' => $sub->show_id ]);
        }

        //All thats left of target subs is subs that need to be added
        foreach($targetSubs as $sub) {
            \DB::insert('INSERT INTO subscriptions (email, show_id) values (?, ?)', [$email, $sub]);
        }

        return " { \"message\": \"Success\" }";
    }

    public function RandomPoster(Request $request) {
        $poster = exec("ls ./shows | shuf -n 1");
        $url = "./shows/" . $poster . "/poster.jpeg";

        return response()->file($url);
        
    }
}
