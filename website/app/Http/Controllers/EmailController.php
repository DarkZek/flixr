<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Helpers\SpamHelper;

class EmailController extends Controller
{
    public function Subscribe(Request $request) {

        SpamHelper::CheckSpam();

        $email = strtolower($_POST["email"]);

        if (is_null($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            die(" { \"message\": \"Invalid email\" }");
        }

        $email_count = sizeof(\DB::select('SELECT * FROM email_verification WHERE email = :email', ['email' => $email]));

        if ($email_count > 0) {
            die(" { \"message\": \"Email has previous unverified shows\" }");
        }

        $subscriptions = $_POST["subscriptions"];

        if (is_null($subscriptions) || strlen($subscriptions) == 0 || strlen($subscriptions) > 300 || !EmailController::SubscriptionsExist($subscriptions)) {
            die(" { \"message\": \"Invalid subscriptions\" }");
        }

        $code = bin2hex(openssl_random_pseudo_bytes(5));

        \DB::insert('INSERT INTO email_verification (email, subscriptions, code, ip_address) values (?, ?, ?, ?)', [$email, $subscriptions, $code, $_SERVER['REMOTE_ADDR'] ]);

        echo "{ \"message\": \"Success\" }";

        session_write_close();
        fastcgi_finish_request();

        EmailController::EmailVerification($code, $email);
    }

    public static function SubscriptionsExist($subs) {

        $shows_storage = getenv("SHOWS_STORAGE_LOCATION");

        foreach(explode(';', $subs) as $id) {
            if (!file_exists($shows_storage . $id . "/")) {
                //Show doesnt exist
                return false;
            }
        }

        return true;
    }

    public function Verify(Request $request) {
        $code = $_GET["code"];

        if (is_null($code)) {
            die("No code provided");
        }

        $result = \DB::select('SELECT * FROM email_verification WHERE code = :code', ['code' => $code]);

        if (sizeof($result) == 0) {
            die("Invalid code");
        }

        $email = $result[0]->email;
        $subscriptions = $result[0]->subscriptions;

        \DB::delete('DELETE FROM email_verification WHERE code = :code', ['code' => $code]);
        
        foreach(explode(';', $subscriptions) as $id) {
            if ($id === '') {
                continue;
            }

            
            $result = \DB::select('SELECT email FROM subscriptions WHERE email = :email AND show_id = :show_id', ['email' => $email, 'show_id' => $id]);

            if (sizeof($result) != 0) {
                //Already subscribed!
                continue;
            }

            \DB::insert('INSERT INTO subscriptions (email, show_id) values (?, ?)', [$email, $id]);
        }


        $user = \DB::select('SELECT * FROM access_keys WHERE email = :email', ['email' => $email]);

        if (sizeof($user) == 0) {
            //Create user
            $access_code = bin2hex(openssl_random_pseudo_bytes(15));
            \DB::insert('INSERT INTO access_keys (email, code) values (?, ?)', [$email, $access_code]);
        } else {
            $access_code = $user[0]->code;
        }

        //Get subscriptions
        $subscriptions = \DB::select('SELECT * FROM subscriptions WHERE email = :email', ['email' => $email]);

        echo("<script>document.location='/verified?code=" . $access_code . "';</script>");

        session_write_close();
        fastcgi_finish_request();

        //Mail it out
        EmailController::AccessVerification($access_code, $email, $subscriptions);
    }

    public function EmailVerification($code, $email) {
        $mail = new PHPMailer(TRUE);

        try {
            $mail->setFrom('noreply@flixrapp.com', 'The Flixr Team');
            $mail->addAddress(@$email, 'User');
            $mail->isHTML(true);
            $mail->Subject  = 'Your verification link';
            $contents       = file_get_contents(__DIR__ . '/../../../resources/email/EmailVerification.html');
            $contents       = str_replace('%CODE%', $code, $contents);
            $mail->Body     = $contents;
            $mail->AltBody  = 'Your verification link is https://flixrapp.com/verify?code=' . $code;
            $mail->send();
        }
        catch (Exception $e)
        {
            //PHP Mailer Issue
            echo $e->errorMessage();
        }
        catch (\Exception $e)
        {
            //Any other php issue
            echo $e->getMessage();
        }
    }

    public function AccessVerification($code, $email, $subscriptions) {

        //Render page
        ob_start();
        include(__DIR__ . '/../../../resources/email/AccessVerification.html');
        $var = ob_get_contents(); 
        ob_end_clean();

        $mail = new PHPMailer(TRUE);

        try {
            $mail->isHTML(true);
            $mail->setFrom('noreply@flixrapp.com', 'The Flixr Team');
            $mail->addAddress(@$email, 'User');
            $mail->Subject = 'Your access link';
            $mail->AltBody  = 'Your access link is https://flixrapp.com/verified?code=' . $code;
            $mail->Body = $var;
            $mail->send();
        }
        catch (Exception $e)
        {
            //PHP Mailer Issue
            echo $e->errorMessage();
        }
        catch (\Exception $e)
        {
            //Any other php issue
            echo $e->getMessage();
        }
    }
}
