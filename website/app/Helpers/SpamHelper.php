<?php

namespace App\Helpers;

class SpamHelper {
    public static function CheckSpam() {

        SpamHelper::CheckIfBlocked();

        $ip = $_SERVER['REMOTE_ADDR'];

        $entries = \DB::table('email_verification')
            ->where("ip_address", "=", $ip)
            ->count();

        //The user already has 3 subscriptions pending with 3 different emails. They're spamming the DB
        if ($entries > 4) {
            \DB::insert('INSERT INTO blocked_ips (ip_address) values (?)', [$ip]);
            header("HTTP/1.1 407 Proxy Authentication Required");
            die("{ \"message\": \"You're blocked from using this service.\" }");
        }

        return false;
    }

    public static function CheckIfBlocked() {
        $ip = $_SERVER['REMOTE_ADDR'];

        $blocked = \DB::table('blocked_ips')
            ->where("ip_address", "=", $ip)
            ->count() > 0;

        if ($blocked) {
            header("HTTP/1.1 407 Proxy Authentication Required");
            die("{ \"message\": \"You're blocked from using this service.\" }");
        }
    }
}