<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemindersSent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reminders_sent')) {
            Schema::create('reminders_sent', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('email');
                $table->string('show_id');
                $table->string('reminder_stage');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminders_sent');
    }
}
