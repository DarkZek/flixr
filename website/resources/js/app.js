require('./bootstrap');

window.Vue = require("vue");
window.Vuex = require("vuex");

Vue.component(
    'show',
    require("./components/Show.vue").default
);

Vue.component(
    'showslist',
    require("./components/ShowsList.vue").default
);

Vue.component(
    'verifiedshowslist',
    require("./components/VerifiedShowsList.vue").default
);

Vue.component(
    'sourcelist',
    require("./components/SourceShowsList.vue").default
);

Vue.component(
    'verifyemail',
    require("./components/VerifyEmail.vue").default
);

Vue.component(
    'verifiedemail',
    require("./components/VerifiedEmail.vue").default
);

Vue.component(
    'notification',
    require("./components/Notification.vue").default
);

Vue.component(
    'howitworks',
    require("./components/HowItWorks.vue").default
);

Vue.component(
    'app-header',
    require("./components/Header.vue").default
);

Vue.component(
    'app-footer',
    require("./components/Footer.vue").default
);

$(document).ready(function () {

    const app = new Vue({
        el: '#app',
    });

    window.app = app;
});

//Create list of shows
window.shows = [];


//Cookie helper script
$.cookie = function(cname, cvalue) {
    //It just wants the data of the cookie
    if (!cvalue) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
    } else {
        //Set the cookie
        var d = new Date();
        d.setTime(d.getTime() + (100*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}


//
// Dark mode
//

//Get system default
window.darkMode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;

//If its overridden set that instead
window.darkMode = $.cookie("darkModeOverride") == "" ? window.darkMode : ($.cookie("darkModeOverride") == 'dark');

window.enableDarkMode = function() {
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = '/css/darkmode.css';
    link.media = 'all';
    link.id = "dark-mode-stylesheet";
    head.appendChild(link);
};

if (window.darkMode) {
    window.enableDarkMode();
}