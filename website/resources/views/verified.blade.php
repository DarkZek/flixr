<?php

if (!isset($_GET["code"])) {
    header("Location: /");
    die();
}

$code = $_GET["code"];

$user = \DB::select('SELECT * FROM access_keys WHERE code = :code', ['code' => $code]);

if (sizeof($user) == 0) {
    header("Location: /");
    die();
}

use App\Helpers\SpamHelper;
SpamHelper::CheckIfBlocked();

?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include("head")
        <title>Flixr - Update Shows</title>
    </head>
    <body>
        <div id="app">
            <app-header></app-header>
            <br>
            <notification></notification>
            <verifiedemail></verifiedemail>
            <br>
            <div class="container">
                <div class="row" style="padding-bottom: 20px;">
                    <sourcelist></sourcelist>
                    <verifiedshowslist></verifiedshowslist>
                </div>
            </div>
            <app-footer></app-footer>
        </div>
    </body>
    <script>
        <?php

        $result = "window.shows = [";

        $subscriptions = \DB::select('SELECT * FROM subscriptions WHERE email = :email', ['email' => $user[0]->email]);

        foreach($subscriptions as $subscription) {
            $result .= '"' . $subscription->show_id . '",';
        }

        //Remove last comma
        $result = substr($result, 0, strlen($result) - 1);

        $result .= "];";

        print($result);

        ?>
    </script>
</html>
