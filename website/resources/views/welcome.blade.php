<?php

use App\Helpers\SpamHelper;
SpamHelper::CheckIfBlocked();

?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include("head")
        <title>Flixr - The TV Show Reminder</title>
    </head>
    <body>
        <div id="app">
            <howitworks>
                <app-header></app-header>
                <div class="page">
                    <br>
                    <notification></notification>
                    <div class="container">
                        <div class="row" style="padding-bottom: 20px;">
                            <sourcelist></sourcelist>
                            <showslist></showslist>
                        </div>
                    <verifyemail></verifyemail>
                </div>
            </howitworks>
            <app-footer></app-footer>
        </div>
    </body>
</html>
