<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="xsrf" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<link href="/css/app.css" rel="stylesheet">

<!-- Ensure both dark and light mode assets are loaded so that the change is instant -->
<link rel="preload" href="/css/darkmode.css" as="style">

<script src="/js/app.js"></script>
<meta property=”og:image” content=”http://localhost:8880/shows/tt4574334/poster.jpeg”/>
<meta property="og:title" content="Flixr - The TV Show Reminder"/>
<meta property="og:type" content="website" />
<meta property="og:description" content="Flixr is the free reminder tool to track and remind you of upcoming seasons of your favorite tv shows. No account required."/>
<meta property=”og:url” content=”https://flixrapp.com/”/>
<meta property=”og:site_name” content=”Flixr”/>